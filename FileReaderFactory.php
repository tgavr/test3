<?php

namespace test3;

class FileReaderFactory {

	public function getReaderInstance($name) {

		$className = "\\test3\\FileReader\\" . ucfirst($name) . "FileReader";

		return new $className();
	}

	public function openFile($path)
	{
		$extention = end(explode(".", $path));

		return $this->getReaderInstance($extention)->openFile($path);
	}
}