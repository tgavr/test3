<?php

spl_autoload_register(function($class) {

	if (preg_match("/^test3/", $class)) {
		$class = str_replace("test3\\", "", $class);
		$file = str_replace("\\", "/", $class) . ".php";

		require_once $file;
	}
});