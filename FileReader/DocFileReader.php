<?php

namespace test3\FileReader;

class DocFileReader implements IFileReader {
	
	public function openFile($path)
	{
		echo "open word file $path";

		return $this;
	}

	public function execute()
	{
		echo "execute doc file";
	}

}