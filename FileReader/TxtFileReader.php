<?php

namespace test3\FileReader;

class TxtFileReader implements IFileReader {
	
	public function openFile($path)
	{
		echo "open txt file $path";

		return $this;
	}

	public function execute()
	{
		echo "execute txt file";
	}

}