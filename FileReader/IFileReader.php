<?php

namespace test3\FileReader;

interface IFilereader {

	/**
	 * Open file
	 * @param string $path
	 */
	function openFile($path);

	/**
	 * Execute file
	 */
	function execute();
}