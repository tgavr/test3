<?php

require_once "autoload.php";

$factory = new test3\FileReaderFactory();

$txtReader = $factory->getReaderInstance('txt');
$txtReader
	->openFile('test.txt')
	->execute();

$factory
	->openFile('test.txt')
	->execute();